---
date: '2012-10-15'
title: 'FRESHER, JUNIOR, MIDDLE DEVELOPER'
company: 'Pyramid Software & Consulting'
location: 'HCM, VietNam'
range: 'October 2012 - December 2017'
url: 'http://psctelecom.com.vn/'
---

- Analysis, design system from the database to interface.
- Combine with the team to build a core system and handle code.
- Maintain, operate, upgrade, develop system when it was released.
- Support, solving the problems of the customers related to the system.
- Research new technologies to improve the applicable system.
- Linking together the different software systems.
- Sometimes, I am the people who get require immediately from customers.
