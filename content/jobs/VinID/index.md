---
date: '2019-06-17'
title: 'SENIOR DEVELOPER'
company: 'VinID'
location: 'HCM, VietNam'
range: 'June 2019 - Now'
url: 'https://vinid.net/'
---

- Combine with the team to build a core system and handle code.
- Maintain, operate, upgrade, develop system when it was released.
- Research new technologies to improve the applicable system.
- Implement unit tests and integration tests for a few systems.
