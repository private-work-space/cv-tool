---
date: '2017-03-01'
title: 'MIDDLE DEVELOPER'
company: 'Infinity Blockchain Labs'
location: 'HCM, VietNam'
range: 'February 2018 - June 2019'
url: 'https://blockchainlabs.asia/'
---

- Combine with the team to build a core system and implement code.
- Maintain, operate, upgrade, develop manager tool on the desktop.
- Research new technologies to improve the applicable system.
- Linking together the different software systems.
