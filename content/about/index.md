---
title: 'About Me'
avatar: './me.png'
skills:
  - JavaScript
  - HTML & CSS
  - React JS
  - Next JS
  - Node JS
  - GraphQL
  - Kafka
  - Web API
  - Web socket
  - C#
  - MCV
  - SQL Server
  - MySQL
  - Postgresql
  - Mongo DB
---

Hello! I'm Minh Tai, a software engineer based in HCM, VietNam.

PERSONAL INFORMATION

- Full name: NGUYỄN MINH TÀI
- Gender: Male
- Birthday: 03/07/1987 (on ID card), 03/07/1990.
- Address: 88 Pham Ngu Lao street, Go Vap district, Ho Chi Minh city
- Home town: Tan Lap ward, Tan Thanh district, Long An province
- Email: minhtai62it@gmail.com
- Tell: 0976392062

SUMMARY

- Having an engineering degree in [Van Hien University](https://www.vhu.edu.vn/) and more than 9 years of experience in developing software and working with various projects. I experienced with much other position.

- I used to work at three the company such as [Pyramid Soft.ware & Consulting](http://psctelecom.com.vn/), [Infinity Blockchain Labs](https://blockchainlabs.asia/), [VinID](https://vinid.net/).

- I am able to work independently and catch up with new things, new technologies. I can self-research and self-study new techniques. I can adapt to changes in work environments and duties.

CAREER OBJECTIVE

- Cooperation of mutual benefit.
- To work in a dynamic professional environment with a growing company.
- To promote my career and consistent income with capacity.

DEGREE OBTAINED

- Degree: Bachelor of Information Technology
- Subject: Applied Informatics in Management
- Year: 2008 – 2012
- University: Van Hien University

HUMAN SKILL

- Good interpersonal & teamwork skill, and take the Leadership.
- Have sense of responsibility.
- Capability for problem-solving.
- Able to work under high pressure.
- Hardworking

Here are a few technologies I've been working with recently:
