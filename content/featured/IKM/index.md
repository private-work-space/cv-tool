---
date: '2020-01-10'
title: 'Institute for Knowledge Management (Frontend & Backend)'
cover: './ikm.png'
github: ''
external: ''
tech:
  - React JS (Component, Hook form)
  - Material-UI
  - Redux saga
  - Redux hook form
  - Redux toolkit
  - Node JS
showInProjects: true
---

Duty:

- Build a core system, implement authentication, authorized.
- Integrate all tasks from BA.
- Research new technologies to apply to the system.
- Implement & Fix bug on the system.

Scope:

- Manage role of all system.
- Manage topic, question, test build, schedule, calendar.
- Build test system for test talker.
- Export data, report & statistic.
- Integrate draw IO to system.

Customer:

- All staff of OneID
