---
date: '2017-03-01'
title: 'Lottery Tool (App on the desktop)'
cover: './lt.png'
github: ''
external: ''
tech:
  - React JS
  - Redux
  - Redux-persist
  - Redux-saga
  - Electron, Electron-builder
  - Node-sass
  - Node js
  - Kafka
  - Web API
  - Web socket
  - Mongodb
  - MySQL
  - Caching Redis
showInProjects: true
---

Duty:

- Can tracking the history of the lottery system.
- Handle function all function as Set selling time, Start selling, Deposit bounty, Buy tickets, Finished selling...
- Combine with the team to develop, upgrade the current lottery system.
- Research, Apply caching Redis to increase performing of system.

Scope:

- Provides intuitive and flexible user interface
- Manages all function for the manager can run the lotteries system
- Provides easy searching and retrieval of information across user interface

Customer:

- All customer at England
