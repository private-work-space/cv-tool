---
date: '2018-06-01'
title: 'Vinhomes Urban Manager (Frontend)'
cover: './vh.png'
github: ''
external: ''
tech:
  - React JS
  - Material-UI
  - Redux saga
  - Redux form
  - Immutable
showInProjects: true
---

Duty:
- Build a core system, implement authentication, authorized.
- Integrate all tasks from BA.
- Research new technologies to apply to the system.
- Write the unit testing & Integration test to the system.

Scope:

- Manage all information about Vinhomes city.
- Manage all utility of the apartment.
- Manage all messages of the resident.
- Manage all history of notification.
- Manage condition terms.
- Manage all information of the resident.
- Manage all services of the apartment.

Customer:

- All Vinhomes urban
