---
date: '2013-06-15'
title: 'Human Resource Manager (Win forms)'
cover: './hrm.png'
github: ''
external: ''
tech:
  - C#.Net
  - eXpressApp Application
  - SQL SERVER
  - Devexpress UI
  - Devexpress XtraReport at runtime
  - MVC (Model View Controller)
  - Web Service
  - Web API
showInProjects: true
---

Scope:

- Manage staff information, salary information, personal tax income, annual leave day, time attendance ...
- Manage more than 20.000 people.
- Provide easy searching and retrieval of information across the user interface.
- Provides extensive standard and customizable reporting capabilities.
- Enables easy management and maintenance of software modules.

Customer:

- Industrial University of Ho Chi Minh city
- Ho Chi Minh city University of Technology Education
- University of Economics and Law
- Ho Chi Minh city University of Law
- Thu Duc college of technology
- Yersin University
- Banking University
- Da Lat University
- TTC Education Join Sotck Company
