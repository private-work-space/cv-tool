---
date: '2014-01-01'
title: 'Time Attendance (Web forms)'
cover: './ta.png'
github: ''
external: ''
tech:
  - C#.Net
  - Entity Framework
  - SQL SERVER
  - ADO.NET
  - ZK Teco SDK
  - MVC
  - Jqwidgets
  - Kendo
  - Web Service
  - Web API
showInProjects: true
---

Scope:

- Manage time attendance of all staff.
- Read data from the time attendance machine with ZK Teco SDK.
- Export data to the HR system to calculate salary.

Customer:

- Industrial University of Ho Chi Minh city
- Ho Chi Minh city University of Technology and Education
- Yersin Uinversity
- Banking University
- Da Lat University
- TTC Education Join Sotck Company
