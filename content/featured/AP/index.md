---
date: '2016-07-20'
title: 'Article Publishing Manager (Web forms)'
cover: './new.png'
github: ''
external: ''
tech:
  - C#.Net
  - SQL SERVER
  - eXpressApp Aplication
  - Devexpress UI
  - React Js
  - Redux
  - Redux thunk
showInProjects: true
---

Scope:

- Provide an intuitive and flexible user interface.
- Allow the user to create the article, censorship, confirm, publish ...
- Manage all user information, roles.
- Provide extensive standard and customizable reporting capabilities.

Customer:

- Industrial University of Ho Chi Minh city
- Ho Chi Minh city University of Technology and Education
- Yersin Uinversity
- Banking University
- Da Lat University
- TTC Education Join Sotck Company
