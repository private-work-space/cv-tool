---
date: '2012-06-15'
title: 'Asset Management System (Win forms)'
cover: './ams.png'
github: ''
external: ''
tech:
  - VS Code
  - C#.Net
  - Entity Framework
  - MS SQL 2012
  - Devexpress UI
  - Devexpress Report at runtime
showInProjects: true
---

Scope:

- Manage more than 50.000 asset items.
- Provide easy searching and retrieval of information across the user interface.
- Provide extensive standard and customizable reporting capabilities.
- Enable easy management and maintenance of software modules.

Customer:

- Ho Chi Minh city television
- Agribank Jewelry
- Thu Duc college of technology
- Ho Chi Minh city University of Technology Education
