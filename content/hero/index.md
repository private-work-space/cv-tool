---
title: 'Hi, my name is'
name: 'Nguyen Minh Tai'
subtitle: 'I am hardworking, carefully, a very responsible person.'
buttonText: 'Get In Touch'
---

I'm a software engineer based in HCM - VietNam.

Having an engineering degree in Van Hien University and more than 9 years of experience in developing software and working with various projects.
