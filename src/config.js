module.exports = {
  siteTitle: 'Nguyen Minh Tai | Software Engineer',
  siteDescription:
    'Minh Tai is a software engineer based in HCM, who specializes in building websites, applications with webform and winform.',
  siteKeywords: '',
  siteUrl: 'https://www.google.com/',
  siteLanguage: 'en_US',
  googleAnalyticsID: 'UA-45666519-2',
  googleVerification: 'DCl7VAf9tcz6eD9gb67NfkNnJ1PKRNcg8qQiwpbx9Lk',
  name: 'Nguyen Minh Tai',
  location: 'Go Vap, Ho Chi Minh',
  email: 'minhtai62it@gmail.com',
  github: 'https://www.google.com/',
  twitterHandle: '',
  socialMedia: [
    {
      name: 'Linkedin',
      url: 'https://www.linkedin.com/in/tai-nguyen-minh-ba7657176',
    },
  ],

  navLinks: [
    {
      name: 'About',
      url: '/#about',
    },
    {
      name: 'Experience',
      url: '/#jobs',
    },
    {
      name: 'Contact',
      url: '/#contact',
    },
  ],

  navHeight: 100,

  colors: {
    green: '#64ffda',
    navy: '#828282',
    darkNavy: '#828282',
  },

  srConfig: (delay = 200) => ({
    origin: 'bottom',
    distance: '20px',
    duration: 500,
    delay,
    rotate: { x: 0, y: 0, z: 0 },
    opacity: 0,
    scale: 1,
    easing: 'cubic-bezier(0.645, 0.045, 0.355, 1)',
    mobile: true,
    reset: false,
    useDelay: 'always',
    viewFactor: 0.25,
    viewOffset: { top: 0, right: 0, bottom: 0, left: 0 },
  }),
};
